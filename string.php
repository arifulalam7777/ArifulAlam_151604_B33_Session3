<?php
    echo 'This string is single quoted only'; //single quoted String
    echo '<br>';
    echo "This string is double quoted only"; //double quoted String
    echo '<br>';
    //echo 'This string is a single and a double quotes."; // single quoted String & double quoted String show syntex error
 ?>
 
 <?php
   $str = <<<EOD
        This is 1st line \n This is second line.;
        
EOD;
        echo $str;
        echo '<br>';
?>



<?php
echo <<<'EOD'
        This is first line. \nThis is second line.;
EOD;
?>