<?php
    /* First method to create array. */
    $numbers = array( 1, 2, 3, 4, 5);

    foreach( $numbers as $value ) {
        echo "Value is $value <br>";
    }

    /* Second method to create array. */
    $numbers[0] = "one";
    $numbers[1] = "two";
    $numbers[2] = "three";
    $numbers[3] = "four";
    $numbers[4] = "five";

    foreach( $numbers as $value ) {
        echo "Value is $value <br>";
    }

    //First method of associative create array.
    $ages = array("abir"=> 50, "rupak" => 40, "jashim" => 30);
    echo "Age of Abir is ". $ages['abir'].".<br>";
    echo "Age of Rupak is " . $ages['rupak']. ".<br>";
    echo "Age of Jashim is ". $ages['jashim']. ".<br>";
    //Second method of associative create array.
    $ages['abir'] = "high";
    $ages['rupak'] = "medium";
    $ages['jashim'] = "low";
    echo "Age of Abir is ". $ages['abir']. ".<br>";
    echo "Age of Rupak is " . $ages['rupak']. ".<br>";
    echo "Age of Jashim is ". $ages['jashim']. ".<br>";

?>
<?php
    //Multidimensional create array.
$marks = array(
    "abir" => array (
        "physics" => 35,
        "maths" => 30,
        "chemistry" => 39
    ),
    "rupak" => array(
        "physics" => 40,
        "maths" => 35,
        "chemistry" => 38
    ),
    "jashim" => array(
        "physics" => 50,
        "maths" => 39,
        "chemistry" => 37
    )
);
    echo "Marks for Abir in physics : " ;
    echo $marks['abir']['physics'] . "<br />";

    echo "Marks for Rupak in maths : ";
    echo $marks['rupak']['maths'] . "<br />";

    echo "Marks for Jashim in chemistry : " ;
    echo $marks['jashim']['chemistry'] . "<br />";
?>

