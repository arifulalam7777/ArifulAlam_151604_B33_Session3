<?php
    echo "Hello World."; //This is single line comment.
    echo "<br>";
?>

<?php
    echo "Bangladesh is our mother land"; /* This is multiple line comment
                                           Line 1
                                           Line 2
                                           Line 3*/
    echo "<br>";
?>
<?php
    echo "Bangladesh is a beautiful Country."; #This is One –line shell style comment.
    echo "<br>";
?>
