<?php
    echo $x = True; // assign the value TRUE to $x.
    echo "<br>";
?>

<?php
    echo $a = 1234; // decimal number
    echo "<br>";
    echo $a = -123; // a negative number
    echo "<br>";
    echo $a = 0123; // octal number (equivalent to 83 decimal)
    echo "<br>";
    echo $a = 0x1A; // hexadecimal number (equivalent to 26 decimal)
    echo "<br>";
    echo $a = 0b11111111; // binary number (equivalent to 255 decimal)
    echo "<br>";
?>

<?php
    echo $a = 1.234;
    echo "<br>";
    echo $b = 1.2e3;
    echo "<br>";
    echo $c = 7E-10;
    echo "<br>";
?>

