<?php
    // floatval function
    $var_name1="122.50";
    $var_name2="122,50";
    $var_name3="-122 50";
    echo floatval($var_name1)."<br>";
    echo floatval($var_name2)."<br>";
    echo floatval($var_name2)."<br>";
?>
<?php
    // empty function
    $var = 0;
    $str = "Hello world";
    if(empty($var)){
        echo '$var'." is empty or 0 .<br>";
    }else{
        echo '$var'." is not empty or 0 .<br>";
    }
    if(empty($str)){
        echo '$str'." is empty or 0 .<br>";
    }else{
        echo '$str'." is not empty or 0 .<br>";
    }
?>
<?php
    // is_array function
    $var = array ('a', 'b', 'c');
    if(is_array($var)){
        echo "This is array";
    }else{
        echo "This is not array";
    }
    echo "<br>";
?>
<?php
    // is_null function
    $var = TRUE;
    if(is_null($var)){
        echo 'Variable is  NULL';
    }else{
        echo 'Variable is not NULL';
    }
    echo "<br>";
?>
<?php
    // print_r function
    $var1 = "abc";
    $var2 = 123.33;
    print_r($var1);
    echo'<br>';
    print_r($var2);
    echo'<br>';
    $var3 =  array('Subj1'=>'Physics','Subj2'=>'Chemistry','Subj3'=>'Mathematics','Class'=>array(5,6,7,8));
    print_r($var3);
    echo "<br>";
?>
<?php
    // $serialized_data function
    $serialized_data = serialize(array('Math', 'Language', 'Science'));
    echo  $serialized_data . '<br>';
    // Unserialize the data
    $var1 = unserialize($serialized_data);
    // Show the unserialized data;
    var_dump ($var1);
    echo "<br>";
?>
<?php
    // Unset the data
    $var = "abc";
    echo "Befor using unset the value of $var is : " .$var. "<br>";
    unset ($var);
    echo "After using unset the value of $var is : " .$var. '<br>';
?>
<?php
        //var_dump function
        $var_name1=678;
        $var_name2="a678";
        $var_name3="678";
        $var_name4="abc";
        $var_name5=698.99;
        $var_name6=+125689.66;
        echo var_dump($var_name1)."<br>";
        echo var_dump($var_name2)."<br>";
        echo var_dump($var_name3)."<br>";
        echo var_dump($var_name4)."<br>";
        echo var_dump($var_name5)."<br>";
        echo var_dump($var_name6)."<br>";
?>
<?php
    //var_export function
    $var_name1=678;
    $var_name2='a678';
    $var_name3='678';
    echo var_export($var_name1)."<br>";
    echo var_export($var_name2)."<br>";
    echo var_export($var_name3)."<br>";

?>
<?php
    //gettype function
    echo gettype(102).'<br>';
    echo gettype(true).'<br>';
    echo gettype(' ').'<br>';
    echo gettype(null).'<br>';
    echo gettype(array()).'<br>';
    echo gettype(new stdclass()).'<br>';
?>
<?php
    //is_bool fnction
    $var = "TRUE";
    if(is_bool($var)){
        echo 'This is a boolean';
    }else{
        echo 'This is not a boolean';
    }
    echo "<br>";
?>
<?php
    //is_float fnction
    $var = "TRUE";
    if(is_float($var)){
        echo 'This is a float';
    }else{
        echo 'This is not a float';
    }
?>
<?php
    //is_string function
    if (is_string("2663"))
        echo "It is a string\n";
    else
        echo 'It is not a string';
    var_dump(is_string('XYZ'));
    var_dump(is_string("99"));
    var_dump(is_string(123.05));
    var_dump(is_string(false));
?>
<?php
    //is_int function
    $var_name1=678;
    $var_name2="a678";
    if (is_int($var_name1)){
        echo "$var_name1 is Integer<br>" ;
    }else{
        echo "$var_name1 is not an Integer<br>" ;
    }
    if (is_int($var_name2)){
        echo "$var_name2 is Integer<br>" ;
    }else {
        echo "$var_name2 is not Integer<br>";
    }
?>
<?php
    //intval function
    echo intval(102).'<br>';
    echo intval(102.22).'<br>';
    echo intval('102').'<br>';
    echo intval(+102).'<br>';
    echo intval(-102).'<br>';
    echo intval(0102).'<br>';
    echo intval('0002').'<br>';
?>
<?php
    //is_scalar function
    $a = 100;
    if (is_scalar ($a))
        echo $a. ' is a scalar value'.'<br>';
    else
        echo $a.' is not a  scalar value'.'<br>';
    $b = array(1);
    if (is_scalar ($b))
        echo $var2. ' is a scalar value'.'<br>';
    else
        echo  $var2. ' is not a  scalar value'.'<br>';
?>